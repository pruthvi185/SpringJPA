package com.main;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;


@Component
public interface EmployeeRepository extends JpaRepository<Employee, Long>{
	

	
	@Query(value = "SELECT * FROM EMPLOYEE WHERE EMPTYPE = 'permanent'",
		    
		    nativeQuery = true)
		  List<Employee> findByemptype();
	
@Query(value = "SELECT * FROM EMPLOYEE WHERE EMPTYPE = 'contract'",
		    
		    nativeQuery = true)
		  List<Employee> findByemptype1();


    @Query(value = "SELECT count(*) FROM EMPLOYEE ",

    nativeQuery = true)
     int countrecords();
    
    @Query(value = "SELECT * FROM EMPLOYEE where EMPNAME LIKE '%tha%' ",

    	    nativeQuery = true)
	List<Employee> LikeQuery();
   }
