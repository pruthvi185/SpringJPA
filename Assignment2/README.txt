I used Ojdbc8 and hibernate for this assignment
Declared all the dependencies in pom.xml file

1. Enter the path /cds/all to get information about all cds
2. Enter the path /cds/{id} to get that particular cd details
3. Enter path /cds/load to save new cds to database
4. Enter path /cds/update to update existing records
5. Enter path /cds/delete/{cdid} to delete that cd details from DB

I used postman to test all these CRUD operations