package com.main;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;


@Component
public interface EmployeeRepository extends JpaRepository<Employee, Long>{
	

	Employee findByempid(Long id);

	List<Employee> findByempdoj(Date date);

	List<Employee> findByempnameIgnoreCase(String name);

}
